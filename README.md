# Algorithm

## Algorithm :N1 Quicksort recursion Algorithm

sorting on payout

product :100 ta

`test1: 0.592ms`
`test2: 0.482ms`
`test3: 0.49ms`

product : 1000 ta

`test1: 2.05ms`
`test2: 1.732ms`
`test3: 2.289ms`

```
const fs = require('fs');

fs.readFile('./loadboard.json', 'utf8', (err,data)=>{
  const d = JSON.parse(data)
  const arr  = d.JSON.workOpportunities   

  console.time();
  const quickSort = (originalList) => {
    const list = [...originalList]
    if (list.length < 2) {
     return list
  }

  const pivot = list[Math.floor(list.length / 2)].payout.value
  const pivotA = list[Math.floor(list.length / 2)]

  const smaller = list.filter((item) => item.payout.value < pivot)
  const bigger = list.filter((item) => item.payout.value > pivot)

  return [...quickSort(smaller), pivotA, ...quickSort(bigger)]
}
quickSort(arr)
console.timeEnd()

});

```
## Algorithm :N2 Quicksort recursion Algorithm

sorting on payout

product :100 ta

`test1: 0.383ms`
`test2: 0.38ms`
`test3: 0.41ms`

product : 1000 ta

`test1: 15.1485ms`
`test2: 14.927ms`
`test3: 13.594ms`



```
 const fs = require('fs');


fs.readFile('./loadboard1000.json','utf8',(err,data)=>{
// fs.readFile('./loadboard.json','utf8',(err,data)=>{
  const d = JSON.parse(data)
  const arr  = d.JSON.workOpportunities   

console.time()
function quickSort(array) {  
let l = array.length
if (l > 1) {
  let pivot = array[Math.floor(l/2)].payout.value
  let pivotA = array[Math.floor(l/2)]
  let small = []
  let large = []    
  
  for (let i=1; i<l-1; i++) {
    if (array[i].payout.value <= pivot) {
      small.push(array[i])
    } else {
      large.push(array[i])
    }
  }; 
  // console.log(small, large, pivot)
  return quickSort(small).concat(pivotA, quickSort(large))  
} else return array;  
}

const array = quickSort(arr)
console.timeEnd()
})
```




## Algorithm :N3 Merge Sort recursion Algorithm

sorting on payout

product :100 ta

`test1: 0.339ms`
`test2: 0.354ms`
`test3: 0.417ms`

product : 1000 ta

`test1: 2.913ms`
`test2: 4.873ms`
`test3: 3.32ms`



```
const fs = require('fs');


fs.readFile('./loadboard.json','utf8',(err,data)=>{
  const d = JSON.parse(data)
  const arr  = d.JSON.workOpportunities   


console.time();
  const  mergeSort = input=> {
    const {length:arraySize} = input;
    if (arraySize < 2) return input;
    const mid = Math.floor(arraySize/2);
    const sortedLeftArray = mergeSort(input.slice(0,mid));
    const sortedRightArray = mergeSort(input.slice(mid, arraySize));
    return merge(sortedLeftArray, sortedRightArray);
  }

  function merge (left, right){
    let result = []
    while(left.length && right.length){
      if(left[0].payout.value< right[0].payout.value){
        result.push(left.shift())
      }else{
        result.push(right.shift())
      }
    }
    /* Either left/right array will be empty or both */
    return [...result, ...left, ...right];
  }

    mergeSort(arr)
console.timeEnd();

})
```



## Algorithm :N4   JavaScript Default Sort method Algorithm

sorting on payout and totalDuration


product :100 ta

`test1: 0.174ms`
`test2: 0.162ms`
`test3: 0.181ms`

product : 1000 ta

`test1: 1.512ms`
`test2: 2.349ms`
`test3: 2.946ms`




```
 const fs = require('fs');


 fs.readFile('./loadboard.json','utf8',(err,data)=>{
// fs.readFile('./loadboard.json','utf8',(err,data)=>{
  const d = JSON.parse(data)
  const arr  = d.JSON.workOpportunities   

  console.time()

  const sortA = (array) =>{
    return   array.sort((a,b)=> b.payout.value -  a.payout.value)
}
sortA(arr))

 console.timeEnd()
})
```
