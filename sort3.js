const fs = require('fs');

fs.readFile('./loadboard.json', 'utf8', (err,data)=>{
  const d = JSON.parse(data)
  const arr  = d.JSON.workOpportunities   

  console.time();
  const quickSort = (originalList) => {
    const list = [...originalList]
    if (list.length < 2) {
     return list
  }

  const pivot = list[Math.floor(list.length / 2)].payout.value
  const pivotA = list[Math.floor(list.length / 2)]

  const smaller = list.filter((item) => item.payout.value < pivot)
  const bigger = list.filter((item) => item.payout.value > pivot)

  return [...quickSort(smaller), pivotA, ...quickSort(bigger)]
}
quickSort(arr)
console.timeEnd()

});